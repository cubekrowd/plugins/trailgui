package ca.jamiesinn.trailgui.trails;

import java.util.Random;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class EntityEffectTrail extends Trail
{
    private Color color;
    private Random random = new Random();
    private boolean isRedRandom;
    private boolean isBlueRandom;
    private boolean isGreenRandom;
    private boolean isAlphaRandom;

    public EntityEffectTrail(ConfigurationSection config)
    {
        super(config);
        int red = (int) (config.getDouble("color.red", 1) * 255) & 0xff;
        int green = (int) (config.getDouble("color.green", 1) * 255) & 0xff;
        int blue = (int) (config.getDouble("color.blue", 1) * 255) & 0xff;
        int alpha = (int) (config.getDouble("color.alpha", 1) * 255) & 0xff;
        color = Color.fromARGB(alpha, red, blue, green);
        isRedRandom = "random".equals(config.getString("color.red"));
        isGreenRandom = "random".equals(config.getString("color.green"));
        isBlueRandom = "random".equals(config.getString("color.blue"));
        isAlphaRandom = "random".equals(config.getString("color.alpha"));
        loadType(config.getString("type"));
    }

    @Override
    protected void loadType(String sType)
    {
        this.type = Particle.valueOf(sType);
    }

    @Override
    public void justDisplay(Player player)
    {
        if (!displayEvent(getName(), getDisplayLocation(), getAmount(), cooldown, getSpeed(), getRange(), type).isCancelled())
        {
            int red = color.getRed();
            int green = color.getGreen();
            int blue = color.getBlue();
            int alpha = color.getAlpha();
            // NOTE(traks): This is how vanilla used to colour some particles
            // before coloured particles got their colour options
            if (isRedRandom) red = (int) (random.nextGaussian() * 255) & 0xff;
            if (isGreenRandom) green = (int) (random.nextGaussian() * 255) & 0xff;
            if (isBlueRandom) blue = (int) (random.nextGaussian() * 255) & 0xff;
            if (isAlphaRandom) alpha = (int) (random.nextGaussian() * 255) & 0xff;
            Color finalColor = Color.fromARGB(alpha, red, green, blue);
            player.getWorld().spawnParticle(type, player.getLocation().add(0.0D, displayLocation, 0.0D), amount, maxOffset, maxOffset, maxOffset, speed, finalColor);
        }
    }
}
