# TrailGUI 

A fork/continuation of [TrailGUI](https://www.spigotmc.org/resources/trailgui.1091/) maintained by CubeKrowd's development team.
Note that we use Paper and develop with Paper's APIs, so this plugin may inadvertently require Paper to function.

In version 6.17 some breaking changes to the configuration file were made:
- Many of the particle type names were changed to Mojang's names, because Spigot now uses them instead of its own ones.
- The `dustColor` option's `RED`, `GREEN` and `BLUE` fields with hexadecimal values were renamed to `red`, `green` and `blue` and now use values between 0 and 1.
- It may help to know how the default config file changed; look [here](https://gitlab.com/cubekrowd/plugins/trailgui/-/commit/fd5287e3a2196c39a1b0e9489ceacb97631e4b11).
